
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .models import  Media




# Create your views here.
@login_required
def home(request):
    context={}

    return  render(request,"tmpl1/accounts/home.html", context)

@login_required
def post(request):
    context={}
    context['error'] =0
    if request.method == 'POST':
        context['request']={}
        context['request']['text']=request.POST.get('text','')
        context['request']['path']=request.FILES.get('path','')
        if context['error'] == 0:
            media=Media(text=context['request']['text'],owner=request.user)
            media.save()
            context['register'] = 1
    return render(request, "tmpl1/accounts/post.html", context)

@login_required
def report(request):
    context={}
    return render(request,"fileupload/picture_form.html", context)

@login_required
def comment(request):
    context={}
    return render(request,"tmpl1/accounts/post.html", context)

