from django.db import models
from client.models import User

# Create your models here.

def upload_to(instance,filename):
    return '%s/%s/%s'%("images/post/",instance.post.id,filename)

class Media(models.Model):
    text= models.TextField(null=True,blank=True)
    owner = models.ForeignKey(User,on_delete=models.CASCADE)
    published = models.BooleanField(default=False)
    disabled =  models.BooleanField(default=False)

class Report(models.Model):
    text = models.CharField(max_length=50)
    media = models.ForeignKey(Media,on_delete=models.CASCADE)
    owner = models.ForeignKey(User,on_delete=models.CASCADE)

class Comment(models.Model):
    text = models.CharField(max_length=50)
    media = models.ForeignKey(Media,on_delete=models.CASCADE)
    owner = models.ForeignKey(User,on_delete=models.CASCADE)