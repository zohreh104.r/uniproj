from django.contrib import admin
from .models import User
from django.contrib.auth.admin import UserAdmin
# Register your models here.

UserAdmin.list_display += ('is_active','is_superuser',)
UserAdmin.fieldsets += (('Extra Fields',{'fields': ('image', 'cellphone')}),)

admin.site.register(User, UserAdmin)