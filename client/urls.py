from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^login/$', views.Login, name="login"),
    url(r'^logout/$', views.Logout, name="logout"),
    url(r'^changepassword/$', views.changepassword,name="changepassword"),
    url(r'^signup/$', views.signup,name="signup"),
    url(r'^editprofile/$', views.editprofile, name="editprofile"),
    url(r'^friends/$', views.friends, name="friends"),
    url(r'^blocklist/$',views.blocklist,name="blocklist"),



]
