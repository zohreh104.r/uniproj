from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.

def upload_to(instance,filename):
    return '%s/%s/%s/%s'%("users/",instance.id,instance.username,filename)
class User(AbstractUser):
    image = models.ImageField(upload_to=upload_to,blank=True,default="/avatar.png")
    cellphone = models.CharField(max_length=15,unique=True,null=True,blank=True)


class BlockedList(models.Model):
    owner = models.ForeignKey(User,on_delete=models.CASCADE)
    user = models.ForeignKey(User,related_name="block_user",on_delete=models.CASCADE)

class Friends(models.Model):
    owner = models.ForeignKey(User,on_delete=models.CASCADE)
    user = models.ForeignKey(User,related_name="my_friend",on_delete=models.CASCADE)


class Direct(models.Model):
    owner = models.ForeignKey(User,on_delete=models.CASCADE)
    user = models.ForeignKey(User,related_name="who_send_message",on_delete=models.CASCADE)
    text = models.TextField()

class notifiction(models.Model):
    text = models.TextField()
    owner = models.ForeignKey(User,on_delete=models.CASCADE)
    read = models.BooleanField(default=False)
