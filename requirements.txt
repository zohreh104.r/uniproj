django==2.0.2
Pillow==5.0.0
django_jalali==3.1.0
jdatetime==3.5.0
mysqlclient==1.4.4
pytz==2019.2
wheel==0.33.4