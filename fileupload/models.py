# encoding: utf-8
from django.db import models
from socialmedia.models import Media

def upload_to(instance,filename):
    return '%s/%s/%s'%("images/post/",instance.post.id,filename)

class Picture(models.Model):
    """This is a small demo using just two fields. The slug field is really not
    necessary, but makes the code simpler. ImageField depends on PIL or
    pillow (where Pillow is easily installable in a virtualenv. If you have
    problems installing pillow, use a more generic FileField instead.

    """
    file = models.FileField(upload_to=upload_to)
    slug = models.SlugField(max_length=50, blank=True)
    post = models.ForeignKey(Media,on_delete=models.CASCADE,blank=True)
    def __str__(self):
        return self.file.name

    @models.permalink
    def get_absolute_url(self):
        return ('upload-new', )

    def save(self, *args, **kwargs):
        self.slug = self.file.name
        super(Picture, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        """delete -- Remove to leave file."""
        self.file.delete(False)
        super(Picture, self).delete(*args, **kwargs)
