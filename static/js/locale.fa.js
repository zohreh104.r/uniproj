/*
 * jQuery File Upload Plugin Localization Example 6.5.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2012, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/*global window */

window.locale = {
    "fileupload": {
        "errors": {
            "maxFileSize": "فایل شما خیلی بزرگ است",
            "minFileSize": "فایل شما خیلی کوچک است",
            "acceptFileTypes": "فایل شما قابل پذیرش از سرور نمیباشد",
            "maxNumberOfFiles": "تعداد فایل های شما به بیشترین حد خود رسیده است",
            "uploadedBytes": "حجم آپلود فایل ها زیاد است",
            "emptyResult": "فایل آپلود شده شما خالی است"
        },
        "error": "خطا",
        "start": "شروع",
        "cancel": "رد کردن",
        "destroy": "حذف"
    }
};
